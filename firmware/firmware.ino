// firmware V3.3.1
// mutantC v3

#include <Keypad.h>
#include <Keypad_I2C.h>
#include <HID-Project.h>
#include <Wire.h>

const int LED_1 = 10;
const int LED_2 = 6;
const int POWER_MAIN =15;
const int POWER_OFF =A0;
const int POWER_EX =14;
const int POWER_LCD =16;
const int POWER_SENSORS =0;
const int BUZZER =A2;
const int PI_STATE =A3;
const int BATTERY_MESUREMENT =A9;
const int DOCK =1;

// Thumbstick, set pin numbers for the five buttons:
const int upButton = 7;
const int downButton = A1;
const int leftButton = 5;
const int rightButton = 8;
const int mouseButton = 4;

// Switch Stats
static bool keymapState = 0;
static bool expansionState = 0;
static bool lcdState = 0;
static bool sensorState = 0;
static bool keybatteryState = 0;
bool powerstate = 0;

// For Thumbstick
int range = 10;              // output range of X or Y movement; affects movement speed
int responseDelay = 10;     // response delay of the mouse, in ms

// For Battery Voltage
int value = 0;
float voltage;

// For Keypad
const byte ROWS = 5;
const byte COLS = 11;

/* Temperature sensor offset */
#define TEMP_OFFSET -7
/* Temperature in Kelvins when to power off */
static uint16_t temp_thresh = 323;
/* Last temperature measured */
static uint16_t last_temp;

// For Keypad
#define I2CADDR 0x20

#define KEYS_TILDE      KEY_TILDE|MOD_LEFT_SHIFT       /* ~ */
#define KEYS_EXCL       KEY_1|MOD_LEFT_SHIFT           /* ! */
#define KEYS_AT         KEY_2|MOD_LEFT_SHIFT           /* @ */
#define KEYS_HASH       KEY_3|MOD_LEFT_SHIFT           /* # */
#define KEYS_DOLLAR     KEY_4|MOD_LEFT_SHIFT           /* $ */
#define KEYS_PERCENT    KEY_5|MOD_LEFT_SHIFT           /* % */
#define KEYS_CARET      KEY_6|MOD_LEFT_SHIFT           /* ^ */
#define KEYS_AMP        KEY_7|MOD_LEFT_SHIFT           /* & */
#define KEYS_ASTERISK   KEY_8|MOD_LEFT_SHIFT           /* * */
#define KEYS_LRBRACKET  KEY_9|MOD_LEFT_SHIFT           /* ( */
#define KEYS_RRBRACKET  KEY_0|MOD_LEFT_SHIFT           /* ) */
#define KEYS_UNDERSCORE KEY_MINUS|MOD_LEFT_SHIFT       /* _ */
#define KEYS_PLUS       KEY_EQUAL|MOD_LEFT_SHIFT       /* + */
#define KEYS_LCBRACKET  KEY_LEFT_BRACE|MOD_LEFT_SHIFT  /* { */
#define KEYS_RCBRACKET  KEY_RIGHT_BRACE|MOD_LEFT_SHIFT /* } */
#define KEYS_VBAR       KEY_BACKSLASH|MOD_LEFT_SHIFT   /* \ */
#define KEYS_COLON      KEY_SEMICOLON|MOD_LEFT_SHIFT   /* : */
#define KEYS_DQUOTE     KEY_QUOTE|MOD_LEFT_SHIFT       /* " */
#define KEYS_LABRACKET  KEY_COMMA|MOD_LEFT_SHIFT       /* < */
#define KEYS_RABRACKET  KEY_PERIOD|MOD_LEFT_SHIFT      /* > */
#define KEYS_QMARK      KEY_SLASH|MOD_LEFT_SHIFT       /* ? */

#define POWEROFF              0xffff
#define KMAP_SWITCH           0xfffe
#define EXPANSION_SWITCH      0xffee
#define LCD_SWITCH            0xfeee
#define SENSOR_SWITCH         0xeffe
#define KMOUSE_LEFT           0xeefe
#define KMOUSE_RIGHT          0xefee
#define KBATTERY_SWITCH       0xefef
#define KBUZZER               0xffef


static uint16_t keymapSymbols[] = {
    KMOUSE_RIGHT,   LCD_SWITCH,  EXPANSION_SWITCH,   SENSOR_SWITCH,  KBATTERY_SWITCH,        KBUZZER,           POWEROFF,             'E',            'E',    KMAP_SWITCH,  KMOUSE_LEFT,
        KEY_ESC,    KEYS_EXCL,           KEYS_AT,       KEYS_HASH,      KEYS_DOLLAR,    KEYS_PERCENT,     KEYS_CARET,       KEYS_AMP, KEYS_LRBRACKET, KEYS_RRBRACKET,      KEY_LEFT,
        KEY_TAB,    KEYS_VBAR,            KEY_UP, KEYS_UNDERSCORE,       KEYS_TILDE,      KEYS_COLON,  KEY_SEMICOLON,    KEYS_DQUOTE,      KEY_QUOTE,      KEY_TILDE,     KEY_RIGHT,
  KEY_CAPS_LOCK,          'E',          KEY_DOWN,             'E',   KEY_LEFT_BRACE, KEY_RIGHT_BRACE, KEYS_LCBRACKET, KEYS_RCBRACKET, KEYS_LABRACKET, KEYS_RABRACKET, KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,         KEYS_PLUS,       KEY_MINUS,        KEY_SLASH,   KEYS_ASTERISK,      KEY_EQUAL,      KEY_COMMA,     KEY_PERIOD,     KEYS_QMARK,     KEY_ENTER
};

static uint16_t keymapAlpha[] = {
    KMOUSE_RIGHT,   LCD_SWITCH,   EXPANSION_SWITCH,   SENSOR_SWITCH,   KBATTERY_SWITCH,   KBUZZER,   POWEROFF,   'E',   'E', KMAP_SWITCH,    KMOUSE_LEFT,
          KEY_1,        KEY_2,              KEY_3,           KEY_4,             KEY_5,      KEY_6, KEY_7, KEY_8, KEY_9,       KEY_0,        KEY_LEFT,
          KEY_Q,        KEY_W,              KEY_E,           KEY_R,             KEY_T,      KEY_Y, KEY_U, KEY_I, KEY_O,       KEY_P,       KEY_RIGHT,
  KEY_CAPS_LOCK,        KEY_A,              KEY_S,           KEY_D,             KEY_F,      KEY_G, KEY_H, KEY_J, KEY_K,       KEY_L,   KEY_BACKSPACE,
  KEY_LEFT_CTRL, KEY_LEFT_GUI,              KEY_Z,           KEY_X,             KEY_C,      KEY_V, KEY_B, KEY_N, KEY_M,   KEY_SPACE,       KEY_ENTER
};

static char dummyKeypad[ROWS][COLS] = {
  { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10},
  {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},
  {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32},
  {33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43},
  {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54}
};

byte rowPins[ROWS] = {11, 15, 2, 4, 5};                     //connect to the row pinouts of the keypad
byte colPins[COLS] = {6, 9, 8, 7, 10, 12, 13, 14, 3, 1, 0}; //connect to the column pinouts of the 

//initialize an instance of class NewKeypad
Keypad_I2C Dummy( makeKeymap(dummyKeypad), rowPins, colPins, ROWS, COLS, I2CADDR, PCF8575); 


//---------------------------------------------------------------------------------------------
void setup(void){

  // keyboard not working properly if this is ON. maybe somthing needs to be done in I2C line.
  // Slow down CPU about 8mA
//  noInterrupts();
//  CLKPR = 0x80;  // enable change of the clock prescaler
//  CLKPR = 0x04;  // divide frequency by 256
//  interrupts();

  // Set up Serial and I2C
  Wire.begin();
//  Serial.begin(9600);
  
  // Set PINS
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(POWER_MAIN, OUTPUT);
  pinMode(POWER_OFF, INPUT_PULLUP);
  pinMode(POWER_EX, OUTPUT);
  pinMode(POWER_LCD, OUTPUT);
  pinMode(POWER_SENSORS, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(BATTERY_MESUREMENT, INPUT_PULLUP);
  pinMode(PI_STATE, INPUT_PULLUP);
  pinMode(upButton, INPUT_PULLUP);
  pinMode(downButton, INPUT_PULLUP);
  pinMode(leftButton, INPUT_PULLUP);
  pinMode(rightButton, INPUT_PULLUP);
  pinMode(mouseButton, INPUT_PULLUP);

  // Set Switch State
  digitalWrite(POWER_MAIN, HIGH);
  digitalWrite(POWER_EX, LOW);
  digitalWrite(POWER_LCD, LOW);
  digitalWrite(SENSOR_SWITCH, LOW);
  digitalWrite(BUZZER, LOW);
  powerstate = digitalRead(POWER_OFF);

  // Set up Keyboard and Mouse
  Mouse.begin();
  Dummy.begin( );
  
  // Compensate for the slow CPU 
  Dummy.setHoldTime(1);
  Dummy.setDebounceTime(0);
  Dummy.addEventListener(keypadEvent);
  delay(1000);
}


//--------------------------------------------------------------
//void fadeOn(unsigned int time,int increament){
//for (byte value = 0 ; value < 255; value+=increament){
//analogWrite(LED_2, value);
//delay(time/(255/5));
//}
//}
//void fadeOff(unsigned int time,int decreament){
//for (byte value = 255; value >0; value-=decreament){
//analogWrite(LED_2, value);
//delay(time/(255/5));
//}
//}

static void notification(void)
{
  digitalWrite(LED_2, LOW);
  delay(25);
  digitalWrite(LED_2, HIGH);
  delay(25);
  digitalWrite(LED_2, LOW);
  delay(25);
  notificationBuzzer();
}

static void notificationBuzzer(void)
{
  digitalWrite(BUZZER, HIGH);
  delay(50);
  digitalWrite(BUZZER, LOW);
  delay(50);
  digitalWrite(BUZZER, HIGH);
  delay(50);
  digitalWrite(BUZZER, LOW);
}
static void poweroff(void)
{
//  uint8_t i;

  /* Ask RPi to powerdown */
  Keyboard.write(CONSUMER_POWER);

//  /* Wait for about 16 seconds */
//  for (i = 0; i < 20; i++) {
//    notification();
//  }
 // delay(16000);
  /* And finally turn off the power */
  digitalWrite(POWER_MAIN, LOW);
//  Serial.println("poweroffOFF");
}

static void power_expansion(void)
{
  notification();
  
  if(expansionState){
    digitalWrite(POWER_EX, LOW);
    expansionState = 0;
  } else {
    digitalWrite(POWER_EX, HIGH);
    expansionState = 1;
  }
}

static void power_sensors(void)
{
  notification();
  
  if(sensorState){
    digitalWrite(POWER_SENSORS, LOW);
    sensorState = 0;
  } else {
    digitalWrite(POWER_SENSORS, HIGH);
    sensorState = 1;
  }
}

static void power_lcd(void)
{
  notification();

  if(lcdState){
    digitalWrite(POWER_LCD, LOW);
    lcdState = 0;
  } else {
    digitalWrite(POWER_LCD, HIGH);
    lcdState = 1;
  }
}

static void battery_state(void)
{
  notification();

  if(keybatteryState){
    read_voltage();
    keybatteryState = 0;
  } else {
    read_voltage();
    keybatteryState = 1;
  }
}

static void read_voltage(void)
{
  // Get Battery voltage
  value = analogRead(BATTERY_MESUREMENT);
  voltage = value * 5.0/1023;
//  Serial.print("Voltage= ");
//  Serial.println(voltage);
  delay(200);
  return voltage;
}

static void read_temp(void)
{
  /* Set reference to 2.56V and MUX to temp sensor */
  ADMUX = (1<<REFS1) | (1<<REFS0) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0);
  ADCSRB = (1<<MUX5);
  /* Set ADC prescaler and enable ADC */
  ADCSRA = (1<<ADPS2) | (1<<ADPS1) | (1<<ADEN);
  /* The CPU is slow so we do not have to wait here */
  /* Start an ADC conversion */
  ADCSRA |= (1<<ADSC);
  /* Wait for ADC conversion to finish */
  while ((ADCSRA & (1<<ADSC)));

  last_temp = ADCW + TEMP_OFFSET;

  if (last_temp > temp_thresh)
    poweroff();
}

//-----------------------------------------------------------------------------------------

/*
 * Simple UART protocol for setting and getting data.
 * The message consists of a few bytes followed by a newline.
 *
 * To setup the serial port on RPi use:
 *
 * stty -F /dev/ttyACM0 9600 raw -clocal -echo icrnl
 *
 * Then you can read results and write commands with:
 *
 * cat /dev/ttyACM0
 * cat > /dev/ttyACM0
 *
 * cmd[0]:
 * ? - get value
 * ! - set value
 *
 * cmd[1]
 * T - internal temperature in Kelvin
 * P - poweroff threshold temperature in Kelvin
 * V - mutant hardware revision
 *
 * cmd[2] - cmd[4]
 *  - optional command parameter
 *
 * '\n' - message end
 */

static void write_u16_res(char type, uint16_t val)
{
  Serial.write(type);
  Serial.print(val);
  Serial.write('\n');
}

static void processSerial(void)
{
  // Sends info via TTL 
  if (Serial.available()) {
    int inByte = Serial.read();
    if (inByte == '+'){
      Serial.println((String)"mutantC_Version = 3.3");
      Serial.println((String)"Temp = " + last_temp);
      Serial.println((String)"BatteryVoltage = " + voltage + "v");
      Serial.println((String)"DisplayState = " + lcdState);
      Serial.println((String)"SensorState = " + sensorState);
      Serial.println((String)"ExpansionState = " + expansionState);
    }
  }
  static uint8_t cmd[6];
  static uint8_t lb = 0;;
  int b;

  while ((b = Serial.read()) > 0) {
    cmd[lb] = b;

    if (cmd[lb] == '\n')
      break;

    lb = (lb + 1) % 6;
  }

  if (cmd[lb] != '\n')
    return;

  switch (cmd[0]) {
  case '?':
    switch (cmd[1]) {
    case 'T':
      write_u16_res('T', last_temp);
    break;
    case 'P':
      write_u16_res('P', temp_thresh);
    break;
    case 'V':
      write_u16_res('V', 3.3);
    break;
    }
  break;
  case '!':
    switch (cmd[1]) {
      case 'P':
        cmd[lb] = 0;
        temp_thresh = atoi(cmd+2);
      break;
    }
  break;
  }

  lb = 0;
}

//-------------------------------------------------------------

static void selectAlphabet(void)
{
  digitalWrite(LED_1, LOW);
  keymapState = 0;
}

static void selectSymbols(void)
{
  digitalWrite(LED_1, HIGH);
  keymapState = 1;
}

static void switchKeymap(void)
{
  if (keymapState)
    selectAlphabet();
  else
    selectSymbols();
}

static uint16_t mapKey(char key)
{
  if (keymapState)
    return keymapSymbols[key];

  return keymapAlpha[key];
}

static void keypadEvent(KeypadEvent key)
{
  if (Dummy.getState() != PRESSED)
    return;

  uint16_t kcode = mapKey(key);
  
  switch (kcode) {
  case KMAP_SWITCH:
    switchKeymap();
  break;
  case KBATTERY_SWITCH:
    battery_state();
  break;
  case KMOUSE_LEFT:
    mouse_left();
  break;
  case KMOUSE_RIGHT:
    mouse_rgiht();
  break;
  case EXPANSION_SWITCH:
    power_expansion();
  break;
  case SENSOR_SWITCH:
    power_sensors();
  break;
  case LCD_SWITCH:
    power_lcd();
  break;
  case POWEROFF:
    poweroff();
  break;
  }
}

static int left_shift_ref_cnt;

static void processKeys(void)
{
  if (!Dummy.getKeys())
    return;

  int i;

  for (i = 0; i < LIST_MAX; i++) {
    if (!Dummy.key[i].stateChanged)
      continue;

    uint16_t key = mapKey(Dummy.key[i].kchar);

    if (key == KMAP_SWITCH || key == EXPANSION_SWITCH || key == SENSOR_SWITCH || key == LCD_SWITCH || key == KBATTERY_SWITCH)
      continue;

    switch (Dummy.key[i].kstate) {
    case PRESSED:
      if (key & MOD_LEFT_SHIFT) {
        if (!(left_shift_ref_cnt++))
          Keyboard.press(KEY_LEFT_SHIFT);
      }
      Keyboard.press((KeyboardKeycode)key);
    break;
    case RELEASED:
      if (key & MOD_LEFT_SHIFT) {
        if (!(--left_shift_ref_cnt))
          Keyboard.release(KEY_LEFT_SHIFT);
      }
      Keyboard.release((KeyboardKeycode)key);
    break;
    }
  }
}

//----------------------------------------------------------------------------

void thumbstick (void){
  
  // read the thumbstick buttons:
  int upState = digitalRead(upButton);
  int downState = digitalRead(downButton);
  int rightState = digitalRead(rightButton);
  int leftState = digitalRead(leftButton);

  // calculate the movement distance based on the button states:
  int  xDistance = (leftState - rightState) * range;
  int  yDistance = (upState - downState) * range;

  // if X or Y is non-zero, move:
  if ((xDistance != 0) || (yDistance != 0)) {
    Mouse.move(xDistance, yDistance, 0);
  }

  // a delay so the mouse doesn't move too fast:
  delay(responseDelay);
}


void mouse_left (void){
  Mouse.press(MOUSE_LEFT);
  delay(responseDelay);
  Mouse.release(MOUSE_LEFT);
}

void mouse_rgiht (void){
  Mouse.press(MOUSE_RIGHT);
  delay(responseDelay);
  Mouse.release(MOUSE_RIGHT);
}

//-------------------------------------------------------------------------------


void loop(void){

//  notification();
  
  bool pwState = digitalRead(POWER_OFF);
  int piState = digitalRead(PI_STATE);
  if(!pwState){
    poweroff();
  }
//  if(pwState== LOW){
//    poweroff();
//  }
  
  if(piState== LOW){
//    Serial.println("piState");
//    notification();
  }

  // Check Thumbstick state
  thumbstick ();
  
  // Check Keyboard state
  processKeys();



  // Read temperature every few seconds
//  static uint16_t i = 0;
//  if (i++ == 30000) {
//    read_temp();
//    read_voltage();
//    i = 0;
//  }

  // Check for mutantcl
  // processSerial();
  
}

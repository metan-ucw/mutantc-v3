# mutantC-v3

# Checkout the poject website from [here](https://mutantc.gitlab.io).
### A Raspberry-pi handheld platform with a physical keyboard, Display and Expansion header for custom baords (Like Ardinuo Shield).
### It is fully open-source hardware with MIT. So you can modify or hack it as you wish.
### It is platform like Arduino. You can make your expansion-card like gps, Radio etc.
### So help us to make a Community around it.
### [Parts list](https://gitlab.com/mutantC/mutantc-v3/-/blob/master/%20parts_list).

### [Youtube](http://www.youtube.com/c/mutantC).

Featuers that comes with V3

- Have RTC & buzzer.
- Supports v2 Add-ons.
- A track point to have mouse support with left, right buttons.
- Gyro support using MPU6050 module & presure, temp and humidity support usin BME280 module.
- Able to turn of modules, Display, Add-ons using keyboard button.
- Full poweroff using OS and keyboard key.
- 12 pin docking port beside Pi USB ports with UART, I2C, Charging and GPIO.
- 3D parts are more solid and reduce supports when printing.
- Able to read battery voltage using Pro Micro.
- 2 LED's connected to Pro Micro with PWM support and 1 connected Pi.
- Move the stema/Qt I2c connector bottom so it is acessible from outside.
- It is fully open-source hardware. So you can Hack it as you wish.You can make your expansion-card like gps, Radio etc and attach with it.
- You can use any Raspberry-pi form factor like Asus Tinker Board S / PINE H64 Model B/ Banana Pi BPI-M4B etc. You can use Raspberry-pi zero to 4.
- You can access all the ports of the pi and the back part is attached with 4 screw.
- It can hold a 4" or 3.5" touch screen. Also have a physical keyboard attached via USB.
- 18650 battery with charge and discharge protraction .
- You can use Littlevgl make UI that don't need a OS form here.
- It don't need any custom image of Raspbian. You can use vanilla Raspbian and install the LCD driver, that's it.
- So little parts needed to make one. See the parts_list .
- You can use C Suite Application suite made more touch based device in this. This apps are suitable for small screens. See the C Suite.
- Added Adafruit STEMMA QT and SparkFun qwiic connector.


# Download
### Download the project from [here](https://a360.co/2GGGeJ7).

# Software used
### Autodesk fusion 360 - 3d parts
### Eagle - PCB
### Ardinuo IDE - Device Firmware

<img src="p1.jpg" width="500"> 
<img src="p2.jpg" width="500"> 
<img src="p3.jpg" width="500">
<img src="p4.jpg" width="500"> 
<img src="p5.jpg" width="500"> 
<img src="p6.jpg" width="500"> 

# Feedback
We need your feedback to improve the mutantC.
Send us your feedback through GitLab [issues](https://gitlab.com/groups/mutantC/-/issues).
